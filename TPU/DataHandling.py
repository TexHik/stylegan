import cv2
import numpy as np
import os
import time
from PIL import Image


def detect(image):
    cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
    facesdata = cascade.detectMultiScale(image)
    faces = []
    for x,y,w,h in facesdata:
        faces.append(image[y:y+h,x:x+w])
    return faces

class Images():
    def __init__(self,dir):
        self.files = np.array([dir+i for i in os.listdir(dir)], dtype='str')
        self.lenldr = len(self.files)


    def getImages(self, xsize, ysize,ptr=0, cnt=0):
        startitertime = time.time()
        if (cnt==0):
            cnt = self.lenldr
        try:
            img = (np.array([cv2.resize(cv2.imread(self.files[i+ptr]), (xsize,ysize),interpolation=cv2.INTER_LINEAR) for i in range(cnt-ptr)],dtype='float32')-127.5)/127.5
        except:
            print("Error reading file: ", cnt, ptr)
        return img.reshape(-1,xsize,ysize,3)

    def check(self):
            c = 0
            for i in range(self.lenldr):
                print(c,"|",i,"/",self.lenldr)
                try:
                    img = cv2.imread(self.files[i])
                    if (img.shape[0] < 512 or img.shape[1] < 512):
                        os.remove(self.files[i])
                        c += 1
                    else:
                        img = (np.array([cv2.resize(img, (512, 512), interpolation=cv2.INTER_LINEAR)],dtype='float32') - 127.5) / 127.5
                except:
                    os.remove(self.files[i])
            print("Removed: {}".format(c))
    def show(self,data):
        img = Image.fromarray(data, 'RGB')
        img.show()
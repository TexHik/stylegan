import tensorflow as tf
import numpy as np
import time
import os
import DataHandling, Model

BATCH_SISE =5000 #must be dividable by 4
sessid = np.random.randint(0,10000)
Images = DataHandling.Images("./animehq/")
print("Sessid: ",sessid)
assert 'TPU_NAME' in os.environ, 'ERROR: Not connected to a TPU runtime; please see the first cell in this notebook for instructions!'
TPU_ADDRESS = 'grpc://10.240.1.2:8470'
print('TPU address is', TPU_ADDRESS)
#tf.enable_eager_execution()
with tf.Session(TPU_ADDRESS) as session:
    print('TPU devices:')
    print(session.list_devices())
    GAN = Model.StyleGAN(session,resolution=8)
    GAN.loadweights('gs://texbuck-1/saves')
    for epoch in range(20):
        startepochttime = time.time()
        for i in range(int(Images.lenldr/BATCH_SISE)-1):
            startitertime = time.time()
            dataset = Images.getImages(GAN.resolution, GAN.resolution, i*BATCH_SISE, (i+1)*BATCH_SISE,)
            print("Training: ",dataset.shape)
            print(session.run(GAN.generator.layers[1].trainable_variables[0][0][0]))
            print(session.run(GAN.discriminator.layers[1].trainable_variables[0][0][0][0][0]))
            GAN.train_step(tf.random.normal([BATCH_SISE,GAN.fmap_max],stddev=0.05), dataset)
            print('Iteration: {}/{},  Time: {}, Optimizing: {}'.
                  format(i,int(Images.lenldr/BATCH_SISE),time.time()-startitertime,GAN.optimizing))
            GAN.saveweights('gs://texbuck-1/saves')
            print("Saving")
        print('\n----Epoch: {}, Time: {}\n'.
              format(epoch,time.time()-startepochttime))

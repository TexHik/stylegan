import tensorflow as tf
import tensorflow.keras.backend as bkd
from tensorflow.keras import layers

from tensorflow.keras.regularizers import Regularizer
import numpy as np
import cv2
import os
import wassestain
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

class adaptive_layer_instance_norm(tf.keras.layers.Layer):
    def __init__(self, use_bias=False, use_gamma=False):
        self.use_bias = use_bias
        self.use_gamma = use_gamma
        super(adaptive_layer_instance_norm, self).__init__()
    def build(self, input_shape):
        if(self.use_bias):
            self.biases = self.add_variable("betha",shape=input_shape[-1:], initializer='zeros', trainable=True, regularizer=None,constraint=None)

        if (self.use_gamma):
            self.gamma = self.add_variable("gamma",shape=input_shape[-1:], initializer='ones', trainable=True, regularizer=None,
                                          constraint=None)
        super(adaptive_layer_instance_norm, self).build(input_shape)
    def call(self, input, gamma=None, beta=None):
            ln_mean, ln_variance = tf.nn.moments(input, axes=[1, 2, 3], keepdims=True)
            x_ln = (input - ln_mean) / (tf.sqrt(ln_variance + 1e-5))
            x_ln = tf.clip_by_value(x_ln, clip_value_min=-1.0, clip_value_max=1.0)

            ins_mean, ins_variance = tf.nn.moments(input, axes=[1, 2], keepdims=True)
            x_ins = (input - ins_mean) / (tf.sqrt(ins_variance + 1e-5))
            x_ins = tf.clip_by_value(x_ins, clip_value_min=-1.0, clip_value_max=1.0)

            rho = tf.clip_by_value(input, clip_value_min=0.0, clip_value_max=1.0)

            x_hat = rho * x_ins + (1 - rho) * x_ln
            if gamma!=None:
                x_hat = x_hat * gamma
            if (self.use_gamma):
                x_hat *= self.gamma
            if beta!=None:
                x_hat = x_hat + beta
            if (self.use_bias):
                x_hat+=self.biases
            return x_hat

class AddNoise(tf.keras.layers.Layer):
    def __init__(self):
        super(AddNoise, self).__init__()
    def build(self, input_shape):
        super(AddNoise, self).build(input_shape)

    def call(self, x):
        return x+tf.random.normal(tf.shape(x),mean=0.0,stddev=0.04, dtype=x.dtype)

class CoreLayer(tf.keras.layers.Layer):
    def __init__(self, size, chanells):
        self.size = size
        self.chanells = chanells
        super(CoreLayer, self).__init__()
    def build(self,input_shape):
        self.core = self.add_weight(shape=[self.size,self.size,self.chanells], initializer='ones', regularizer=None,
                                         constraint=None, name='gamma', trainable=True,dtype=tf.float32)


        super(CoreLayer, self).build(input_shape)
    def call(self, input):
        batch_size = tf.shape(input)[0]
        self.data = tf.tile(self.core,multiples=[batch_size, 1, 1])
        self.data = tf.reshape(self.data,[-1,self.size,self.size,self.chanells])
        return self.data
class Epiloge(tf.keras.layers.Layer):
    def __init__(self,use_norm=True,name="epiloge"):
        self.layername = name
        self.use_norm = use_norm
        super(Epiloge, self).__init__()
    def build(self,input_shape):

        super(Epiloge, self).build(input_shape)
    def call(self, x):
        x = layers.LeakyReLU()(x)
        if(self.use_norm):
            x = adaptive_layer_instance_norm(use_bias=False,use_gamma=True)(x)
        return x

def lerp_clip(a, b, t): return a + (b - a) * tf.clip_by_value(t, 0.0, 1.0)

def minibatch_stddev_layer(x, group_size=4):
    group_size = tf.minimum(group_size, tf.shape(x)[0])     # Minibatch must be divisible by (or smaller than) group_size.
    s = x.shape                                             # [NHWC]  Input shape.
    y = tf.reshape(x, [group_size, -1, s[1], s[2], s[3]])   # [GMHWC] Split minibatch into M groups of size G.
    y = tf.cast(y, tf.float32)                              # [GMHWC] Cast to FP32.
    y -= tf.reduce_mean(y, axis=0, keepdims=True)           # [GMHWC] Subtract mean over group.
    y = tf.reduce_mean(tf.square(y), axis=0)                # [MHWC]  Calc variance over group.
    y = tf.sqrt(y + 1e-8)                                   # [MHWC]  Calc stddev over group.
    y = tf.reduce_mean(y, axis=[1,2,3], keepdims=True)      # [M111]  Take average over fmaps and pixels.
    y = tf.cast(y, x.dtype)                                 # [M111]  Cast back to original data type.
    y = tf.tile(y, [group_size, s[1], s[2], 1])             # [NHW1]  Replicate over group and pixels.
    return tf.concat([x, y], axis=3)                        # [NHWC]  Append as new fmap.





class WeightNorm(Regularizer):
    def __init__(self, gain=np.sqrt(2)):
        self.gain = gain
    def __call__(self, x):
        if (len(x.shape)==2):
            fan_in = np.prod(x, axis=1)
            std = self.gain / np.sqrt(fan_in)
            return x * std
        elif (len(x.shape)==4):
            fan_in = np.prod(x, axis=2)
            std = self.gain / np.sqrt(fan_in)
            return x * std
        else:
            raise ValueError('Wrong kernel')

    def get_config(self):
        return {'gain': self.gain}





class StyleGAN():
    def __init__(self, sesion,
                 base_channels=4096,
                 fmap_decay=1.0,
                 fmap_max=512,
                 resolution=1024):

        self.base_channels=base_channels
        self.fmap_decay = fmap_decay
        self.fmap_max = fmap_max
        self.resolution = resolution

        self.session = sesion

        self.resolution_log2 = int(np.log2(self.resolution))
        self.num_layers = self.resolution_log2 * 2 - 2


        self.genoptimizer = tf.keras.optimizers.Adam(5e-5,beta_1=0.01)
        self.discoptimizer = tf.keras.optimizers.Adam(5e-5, beta_1=0.01)

        self.kernel_initializer = tf.random_normal_initializer(stddev=0.02, mean=0.0)

        self.generator = self._G()
        self.discriminator = self._D()


        self.optimizing=''
        self.trainstep = 0


        print("Image size: ", self.resolution)
        print(
            "Total count of generator trainable parameters: {}\n"
            "Total count of discriminator trainable parameters: {}\n"
            #"Total count of mapper trainable parameters: {}"
        .format(
                np.sum([np.prod(v.get_shape().as_list()) for v in self.generator.trainable_variables]),
                np.sum([np.prod(v.get_shape().as_list()) for v in self.discriminator.trainable_variables]),
            )
        )
    #MISC
    def nf(self, stage):
        return min(int(self.base_channels / (2.0 ** (stage * self.fmap_decay))), self.fmap_max)


	
    def _G(self):
        inputs = layers.Input(shape=(1,self.fmap_max))
        #Early layers

        x = layers.Dense(self.nf(0)*16,  kernel_regularizer=WeightNorm(gain=np.sqrt(2)/4))(inputs)
        x = layers.Reshape(target_shape=(4,4,self.fmap_max,))(x)
        x = Epiloge(use_norm=True)(x)
        x = layers.Conv2DTranspose(self.nf(0),kernel_size=(3,3), padding='same',use_bias=True, bias_initializer='zeros',
                                   kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(), name='CoreConv3x3')(x)
        x = Epiloge()(x)
        #Main ConvLayers
        def torgb(x,name):
            x = layers.Conv2DTranspose(3, kernel_size=(1,1), padding='same',use_bias=True, bias_initializer='zeros', name=name+'conv_torgb',kernel_initializer = self.kernel_initializer,
                                       kernel_regularizer=WeightNorm(gain=1.0), activation='linear')(x)
            return x
        def UpSampleBlock(x, res, name):
            x = layers.UpSampling2D(size=2)(x)
            x = Epiloge()(x)
            x = layers.Conv2DTranspose(res, kernel_size=(3,3), padding='same',use_bias=True, bias_initializer='zeros', kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(),
                              name='%s' % name + '_conv0_up')(x)
            x = Epiloge()(x)
            x = layers.Conv2DTranspose(res, kernel_size=(3,3), padding='same', use_bias=True, bias_initializer='zeros', kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(),
                              name='%s' % name + '_conv1')(x)
            x = Epiloge()(x)
            return x

        x = torgb(x, 'torgb_init')
        for i in range(3,self.resolution_log2+1):
            img = UpSampleBlock(x, self.nf(i),name='UpSampleBlock_%d'%i)
            img = torgb(img,'ToRGB%d'%i)

            x = layers.UpSampling2D(size=2)(x)

            x = lerp_clip(img, x, 0.0-(self.resolution_log2-i))




        outputs = x
        return tf.keras.Model(inputs=inputs, outputs=outputs)

    def _D(self):
        inputs = layers.Input(shape=(self.resolution, self.resolution, 3,))
        def DownSampleBlock(x, res):
            x = layers.Conv2D(self.nf(res-1), kernel_size=(3,3), padding='same', use_bias=True, bias_initializer='zeros',
                              kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(),
                              name='%dx%d' % (2**res, 2**res) + 'conv0')(x)
            x = Epiloge(use_norm=True)(x)
            x = layers.AveragePooling2D(pool_size=(2, 2))(x)
            x = layers.Conv2D(self.nf(res - 2), kernel_size=(3,3), padding='same', use_bias=True, bias_initializer='zeros',
                              kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(),
                              name='%dx%d' % (2 ** res, 2 ** res) + 'conv1')(x)
            x = Epiloge(use_norm=True)(x)
            return x
        def fromRGB(x,res,name=""):
            x = layers.Conv2D(self.nf(res), kernel_size=(1,1), padding='same', use_bias=True, bias_initializer='zeros',
                                kernel_initializer=self.kernel_initializer,  kernel_regularizer=WeightNorm(),
                                name=name + 'FromRGB_lod')(x)
            return x

        x = fromRGB(inputs,self.resolution_log2,"init_")
        for i in range(self.resolution_log2, 2, -1):
            img = DownSampleBlock(x, i)
            img = fromRGB(img, i-1, str(i-1))

            x = layers.AveragePooling2D(pool_size=(2, 2))(x)
            x = fromRGB(x, i - 1, str(i - 1)+"lerp")
            x = lerp_clip(img, x, 0.0-(self.resolution_log2-i))

        x = minibatch_stddev_layer(x)
        x = layers.Flatten()(x)
        x = layers.Dense(self.nf(0))(x)
        x = Epiloge(use_norm=False)(x)

        x = layers.Dense(1, kernel_initializer=self.kernel_initializer, kernel_regularizer=WeightNorm(gain=1),)(x)
        x = Epiloge(use_norm=False)(x)

        outputs = x
        return tf.keras.Model(inputs=inputs, outputs=outputs)

    def G_wgan_acgan(self, latents):

        fake_images_out = self.generator(latents, training=True)
        fake_scores_out = self.discriminator(fake_images_out, training=True)
        loss = -fake_scores_out
        return loss

    def D_wgangp_acgan(self,  latents, reals,
                       wgan_lambda=10.0,  # Weight for the gradient penalty term.
                       wgan_epsilon=0.001,  # Weight for the epsilon term, \epsilon_{drift}.
                       wgan_target=1.0,  # Target value for gradient magnitudes.
                       cond_weight=1.0):  # Weight of the conditioning terms.

        fake_images_out = self.generator(latents, training=True)
        real_scores_out = self.discriminator(reals, training=True)
        fake_scores_out = self.discriminator(fake_images_out, training=True)
        loss = fake_scores_out - real_scores_out
        epsilon_penalty = tf.square(real_scores_out)
        loss += epsilon_penalty * wgan_epsilon
        return loss

    def train_step(self, inputs, targets):
        self.session.run(tf.global_variables_initializer())
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            disc_loss = self.D_wgangp_acgan(inputs, targets)
            disc_grad = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)
            self.discoptimizer.apply_gradients(zip(disc_grad, self.discriminator.trainable_variables))
            gen_loss = self.G_wgan_acgan(inputs)
            gen_grad = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
            self.genoptimizer.apply_gradients(zip(gen_grad, self.generator.trainable_variables))
        self.trainstep += 1

    def saveweights(self,dir):
        self.generator.save_weights(dir+"/gen/")
        for i in range(len(self.discriminator.layers)):
            l = self.discriminator.layers[i]
            if (l.name[:5]!='tf_op'):
                data = l.get_weights()
                with tf.io.gfile.GFile(dir + "/disc/" + l.name, "wb") as f:
                    np.save(f, data)

    def loadweights(self,dir):
        self.generator.load_weights(dir+"/gen/")

        for i in tf.io.gfile.listdir(dir + "/disc/"):
            try:
                l = self.discriminator.get_layer(name=i)
            except:
                print("Layer:",i," not found")
            else:
                with tf.io.gfile.GFile(dir + "/disc/" + i,"rb") as f:
                        data = np.load(f,allow_pickle=True)
                try:
                    l.set_weights(data)
                except:
                    None


    def saveimage(self,name):
            img = tf.Session().run(self.generator(tf.random.normal([1,self.fmap_max],stddev=0.05)))*127.5+127.5
            cv2.imwrite('gs://texbuck-1/checkpoints/'+str(name)+'.bmp',img[0])


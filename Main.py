import tensorflow as tf
import numpy as np
import time
import os
import DataHandling, Model
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

BATCH_SISE =60 #must be dividable by 4
sessid = np.random.randint(0,10000)
Images = DataHandling.Images("./dataset/animehq/")
print("Sessid: ",sessid)

#по 10 эпохи
GAN = Model.StyleGAN(resolution=4)
GAN.loadweights('./saves')

for epoch in range(5):
    startepochttime = time.time()
    for i in range(int(Images.lenldr/BATCH_SISE)-1):
        startitertime = time.time()
        dataset = Images.getImages(GAN.resolution, GAN.resolution, i*BATCH_SISE, (i+1)*BATCH_SISE)
        with tf.device("/device:GPU:0"):
            loss = GAN.train_step(tf.random.normal([BATCH_SISE,GAN.fmap_max],stddev=0.05), dataset)
        print('Iteration: {}/{},  Time: {},  Loss: {}, Optimizong: {}'.
              format(i,int(Images.lenldr/BATCH_SISE),time.time()-startitertime,loss,GAN.optimizing))
        if(i%30==0):
            GAN.saveweights('./saves')
            GAN.saveimage('{}_{}_{}'.format(sessid,epoch,i))
    print('\n----Epoch: {}, Time: {}\n'.
          format(epoch,time.time()-startepochttime))
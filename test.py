#-*-coding:utf8;-*-
#qpy:3
#qpy:console
from numpy.random import choice
import numpy as np
import random

startbal=400


chance=1/3 #win chance

mult = 1/chance  #win multiplying

betmult = 1.5


startbet=10

iter=80
games = 1000


std = 0
for game in range(games):
    bal = startbal
    bet = startbet
    for i in range(iter):
        bal -= bet
        if (bal<=0):
            break

        if (choice([True,False],p=[chance, 1-chance])):
            bal += bet * mult
            bet=startbet
        else:
            bet= bet*betmult
    std+= (bal-startbal)
print(std/games)